package org.tinygroup.luence.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.tinygroup.luence.LuenceDealer;

public class TestLuenceDealer extends TestCase {
	// String index = "H:\\source\\tiny\\TinySeg\\org.tinygroup.luence\\index";
	// String docsPath = "H:\\source\\tiny\\TinySeg\\org.tinygroup.luence\\src\\test\\resources";

	public void testDealer() {
		String projectDir = System.getProperty("user.dir");
		String indexPath = projectDir+"\\index";
		String docsPath =projectDir+"\\src\\test\\resources";
		
		List<File> files = new ArrayList<File>();
		File file = new File(docsPath);
		addFile(file, files);
		LuenceDealer d = new LuenceDealer(indexPath,files);
		
		try {
			List<String> result= d.find("contents", "重庆");
			if(result.size()>0){
				assertTrue(true);
			}else{
				assertTrue(false);
			}
			for(String s:result){
				System.out.println(s);
			}
			
		} catch (Exception e) {
			assertTrue(false);
		}
		File indexFile = new File(indexPath);
		delete(indexFile);
		
	}
	
	private void delete(File file){
		if(!file.isDirectory()){
			file.delete();
		}else{
			for (File f : file.listFiles()) {
				delete(f);
			}
			file.delete();
		}
		
	}

	private void addFile(File file, List<File> files) {
		if (!file.isDirectory()) {
			files.add(file);
		}
		if(file.listFiles()==null){
			return;
		}
		for (File f : file.listFiles()) {
			addFile(f, files);
		}
	}
}
