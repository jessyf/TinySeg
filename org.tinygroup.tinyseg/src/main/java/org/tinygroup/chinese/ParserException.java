package org.tinygroup.chinese;

/**
 * Created by luog on 15/4/15.
 */
public class ParserException extends  Exception{
    public ParserException(String information){
        super(information);
    }
    public ParserException(Throwable throwable){
        super(throwable);
    }
}
