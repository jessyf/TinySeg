package org.tinygroup.chinese.parsermanager;


import org.tinygroup.binarytree.AVLTree;
import org.tinygroup.chinese.Word;

/**
 * 单词的描述
 * 
 * @author luoguo
 * 
 */
public class WordDescription implements Comparable<WordDescription> {
	private Character character;
	private AVLTree<WordDescription> wordDescriptionAVLTree;
	private Word word=null;

	public Character getCharacter() {
		return character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	public AVLTree<WordDescription> getWordDescriptionAVLTree() {
		return wordDescriptionAVLTree;
	}

	public void setWordDescriptionAVLTree(AVLTree<WordDescription> wordDescriptionAVLTree) {
		this.wordDescriptionAVLTree = wordDescriptionAVLTree;
	}

	public Word getWord() {
		return word;
	}

	public void setWord(Word word) {
		this.word = word;
	}

	public WordDescription(char character) {
		this.character = character;
	}


	public int compareTo(WordDescription o) {
		return character.compareTo(o.character);
	}

}